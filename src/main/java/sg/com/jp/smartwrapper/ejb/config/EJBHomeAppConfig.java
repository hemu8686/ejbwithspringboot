
package sg.com.jp.smartwrapper.ejb.config;

import java.rmi.RemoteException;
import java.util.Properties;

import javax.ejb.CreateException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jndi.JndiTemplate;

import ejb.sessionBeans.smart.smartInterface.SmartInterfaceHome;
import ejb.sessionBeans.smart.smartInterface.SmartInterfaceRemote;

@Configuration
public class EJBHomeAppConfig {

	private static final Log log = LogFactory.getLog(EJBHomeAppConfig.class);

	private static JndiTemplate jndiTemplate;
	@Value("${weblogic.url}")
	private String weblogicUrl;

	@Bean(name = "jndiTemplate")
	public JndiTemplate jndiTemplate() {
		jndiTemplate = new JndiTemplate();
		try {
			log.info("START:jndiTemplate weblogicUrl:" + weblogicUrl);
			Properties map = new Properties();
			map.put(Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory");
			map.put(Context.PROVIDER_URL, weblogicUrl);
			jndiTemplate.setEnvironment(map);
		} catch (Exception e) {
			log.info("Exception :jndiTemplate", e);
		}
		return jndiTemplate;
	}

	public SmartInterfaceRemote getSmartInterfaceRemoteEJB() throws RemoteException, CreateException, NamingException {
		InitialContext ctx = (InitialContext) jndiTemplate.getContext();
		SmartInterfaceHome smartHome = (SmartInterfaceHome) ctx.lookup("smartInterfaceEJB");
		SmartInterfaceRemote sm = (SmartInterfaceRemote) smartHome.create();
		return sm;
	}

	     
}
