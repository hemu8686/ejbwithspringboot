package sg.com.jp.smartwrapper.util;

import java.util.List;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriComponentsBuilder;

import sg.com.jp.smartwrapper.domain.Error;
import sg.com.jp.smartwrapper.domain.Page;

/*
 * Revision History
 * ------------------------------------------------------------------------------------------------------
 * Author			Description												Version			Date
 * ------------------------------------------------------------------------------------------------------
 * MC Consulting	First Version											1.0				27-Mar-2019
 */

public class ResponseEntityUtil {

	public static ResponseEntity<?> created(UriComponentsBuilder ucb, String url, String id) {
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucb.path(url + "/{id}").buildAndExpand(id).toUri());
		return new ResponseEntity<>(headers, HttpStatus.CREATED);
	}

	public static ResponseEntity<?> databaseError(String operation, String tableName) {
		String message = "Unable to " + operation + " in " + tableName + " table, please contact System Administrator.";
		return new ResponseEntity<Error>(new Error(message), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	public static ResponseEntity<?> duplicate(String message) {
		return new ResponseEntity<Error>(new Error(message), HttpStatus.CONFLICT);
	}

	public static ResponseEntity<?> recordNotFound() {
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	public static ResponseEntity<?> success() {
		return new ResponseEntity<>(HttpStatus.OK);
	}

	public static ResponseEntity<?> success(Page page) {
		return new ResponseEntity<Page>(page, HttpStatus.OK);
	}

	public static ResponseEntity<?> success(Object object) {
		return new ResponseEntity<>(object, HttpStatus.OK);
	}

	public static ResponseEntity<?> updated() {
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	public static ResponseEntity<?> validationError(List<Map<String, String>> errors) {
		return new ResponseEntity<Error>(new Error(errors), HttpStatus.BAD_REQUEST);
	}

	public static ResponseEntity<?> validationError(String message) {
		return new ResponseEntity<Error>(new Error(message), HttpStatus.BAD_REQUEST);
	}

}
