package sg.com.jp.smartwrapper.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ejb.sessionBeans.smart.smartInterface.SmartInterfaceRemote;
import sg.com.jp.smart.valueObject.SmartInterfaceInputVO;
import sg.com.jp.smartwrapper.domain.Result;
import sg.com.jp.smartwrapper.ejb.config.EJBHomeAppConfig;
import sg.com.jp.smartwrapper.util.ResponseEntityUtil;

@RestController
@RequestMapping(value = SmartWrapperController.ENDPOINT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class SmartWrapperController {

	public static final String ENDPOINT = "/smart";
	private static final Log log = LogFactory.getLog(SmartWrapperController.class);

	@Autowired
	private EJBHomeAppConfig jndiBean;

	@PostMapping(value = "/createDn")
	public ResponseEntity<?> createDn(@RequestBody SmartInterfaceInputVO smartInterfaceInputVO) {
		Result result = new Result();
		try {
			log.info("START:createDn smartInterfaceInputVO:" + smartInterfaceInputVO.toString());
			SmartInterfaceRemote smartInterfaceUtil = jndiBean.getSmartInterfaceRemoteEJB();
			smartInterfaceUtil.releaseStorageOccupancy(smartInterfaceInputVO);
			result.setSuccess(true);
		} catch (Exception e) {
			result.setSuccess(false);
			log.error("Exception: createDn", e);
		} finally {
			log.info("END: createDn result:" + result.toString());
		}

		return ResponseEntityUtil.success(result.toString());
	}

	@PostMapping(value = "/createUa")
	public ResponseEntity<?> createUa(@RequestBody SmartInterfaceInputVO smartInterfaceInputVO) {
		Result result = new Result();
		try {
			log.info("START:createUa smartInterfaceInputVO:" + smartInterfaceInputVO.toString());
			SmartInterfaceRemote smartInterfaceUtil = jndiBean.getSmartInterfaceRemoteEJB();
			smartInterfaceUtil.markStorageOccupancy(smartInterfaceInputVO);
			result.setSuccess(true);
		} catch (Exception e) {
			result.setSuccess(false);
			log.error("Exception: createUa", e);
		} finally {
			log.info("END: createUa result:" + result.toString());
		}
		return ResponseEntityUtil.success(result.toString());
	}

	
	@GetMapping("test")
	public void test() {
		log.info("***********8TEST*********");
		log.info("send ");
	}
}
