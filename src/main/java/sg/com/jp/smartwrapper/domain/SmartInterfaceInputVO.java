package sg.com.jp.smartwrapper.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class SmartInterfaceInputVO implements Serializable {
	private static final long serialVersionUID = -2073010144960983811L;

	private String vvCd;
	private String userId;
	private String serverNm;
	private String serverIp;
	private String refType;
	private String refNbr;
	private String classNm;
	private String classDesc;
	private String stgType;
	private String stgZone;
	private String actionCd;
	private String estType;
	private BigDecimal tonnage;
	private String occSrcCd;
	private String cntrSize;
	private String cntrType;
	private Integer nbrPkgs;

	private Date atbDttm;
	private Date atuDttm;
	private Date transDttm;
	private String transNbr;
	private boolean isStayingInJP;
	private Long cntrSeqNbr;

	/**
	 * @return the vvCd
	 */
	public String getVvCd() {
		return vvCd;
	}

	/**
	 * @param vvCd the vvCd to set
	 */
	public void setVvCd(String vvCd) {
		this.vvCd = vvCd;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the serverNm
	 */
	public String getServerNm() {
		return serverNm;
	}

	/**
	 * @param serverNm the serverNm to set
	 */
	public void setServerNm(String serverNm) {
		this.serverNm = serverNm;
	}

	/**
	 * @return the serverIp
	 */
	public String getServerIp() {
		return serverIp;
	}

	/**
	 * @param serverIp the serverIp to set
	 */
	public void setServerIp(String serverIp) {
		this.serverIp = serverIp;
	}

	/**
	 * @return the refNbr
	 */
	public String getRefNbr() {
		return refNbr;
	}

	/**
	 * @param refNbr the refNbr to set
	 */
	public void setRefNbr(String refNbr) {
		this.refNbr = refNbr;
	}

	/**
	 * @return the classNm
	 */
	public String getClassNm() {
		return classNm;
	}

	/**
	 * @param classNm the classNm to set
	 */
	public void setClassNm(String classNm) {
		this.classNm = classNm;
	}

	/**
	 * @return the classDesc
	 */
	public String getClassDesc() {
		return classDesc;
	}

	/**
	 * @param classDesc the classDesc to set
	 */
	public void setClassDesc(String classDesc) {
		this.classDesc = classDesc;
	}

	/**
	 * @return the stgType
	 */
	public String getStgType() {
		return stgType;
	}

	/**
	 * @param stgType the stgType to set
	 */
	public void setStgType(String stgType) {
		this.stgType = stgType;
	}

	/**
	 * @return the stgZone
	 */
	public String getStgZone() {
		return stgZone;
	}

	/**
	 * @param stgZone the stgZone to set
	 */
	public void setStgZone(String stgZone) {
		this.stgZone = stgZone;
	}

	/**
	 * @return the refType
	 */
	public String getRefType() {
		return refType;
	}

	/**
	 * @param refType the refType to set
	 */
	public void setRefType(String refType) {
		this.refType = refType;
	}

	/**
	 * @return the estType
	 */
	public String getEstType() {
		return estType;
	}

	/**
	 * @param estType the estType to set
	 */
	public void setEstType(String estType) {
		this.estType = estType;
	}

	/**
	 * @return the tonnage
	 */
	public BigDecimal getTonnage() {
		return tonnage;
	}

	/**
	 * @param tonnage the tonnage to set
	 */
	public void setTonnage(BigDecimal tonnage) {
		this.tonnage = tonnage;
	}

	/**
	 * @return the occSrcCd
	 */
	public String getOccSrcCd() {
		return occSrcCd;
	}

	/**
	 * @param occSrcCd the occSrcCd to set
	 */
	public void setOccSrcCd(String occSrcCd) {
		this.occSrcCd = occSrcCd;
	}

	/**
	 * @return the cntrSize
	 */
	public String getCntrSize() {
		return cntrSize;
	}

	/**
	 * @param cntrSize the cntrSize to set
	 */
	public void setCntrSize(String cntrSize) {
		this.cntrSize = cntrSize;
	}

	/**
	 * @return the nbrPkgs
	 */
	public Integer getNbrPkgs() {
		return nbrPkgs;
	}

	/**
	 * @param nbrPkgs the nbrPkgs to set
	 */
	public void setNbrPkgs(Integer nbrPkgs) {
		this.nbrPkgs = nbrPkgs;
	}

	/**
	 * @return the isStayingInJP
	 */
	public boolean isStayingInJP() {
		return isStayingInJP;
	}

	/**
	 * @param isStayingInJP the isStayingInJP to set
	 */
	public void setStayingInJP(boolean isStayingInJP) {
		this.isStayingInJP = isStayingInJP;
	}

	/**
	 * @return the actionCd
	 */
	public String getActionCd() {
		return actionCd;
	}

	/**
	 * @param actionCd the actionCd to set
	 */
	public void setActionCd(String actionCd) {
		this.actionCd = actionCd;
	}

	/**
	 * @return the atbDttm
	 */
	public Date getAtbDttm() {
		return atbDttm;
	}

	/**
	 * @param atbDttm the atbDttm to set
	 */
	public void setAtbDttm(Date atbDttm) {
		this.atbDttm = atbDttm;
	}

	/**
	 * @return the atuDttm
	 */
	public Date getAtuDttm() {
		return atuDttm;
	}

	/**
	 * @param atuDttm the atuDttm to set
	 */
	public void setAtuDttm(Date atuDttm) {
		this.atuDttm = atuDttm;
	}

	/**
	 * @return the cntrType
	 */
	public String getCntrType() {
		return cntrType;
	}

	/**
	 * @param cntrType the cntrType to set
	 */
	public void setCntrType(String cntrType) {
		this.cntrType = cntrType;
	}

	/**
	 * @return the transDttm
	 */
	public Date getTransDttm() {
		return transDttm;
	}

	/**
	 * @param transDttm the transDttm to set
	 */
	public void setTransDttm(Date transDttm) {
		this.transDttm = transDttm;
	}

	/**
	 * @return the transNbr
	 */
	public String getTransNbr() {
		return transNbr;
	}

	/**
	 * @param transNbr the transNbr to set
	 */
	public void setTransNbr(String transNbr) {
		this.transNbr = transNbr;
	}

	/**
	 * @return the cntrSeqNbr
	 */
	public Long getCntrSeqNbr() {
		return cntrSeqNbr;
	}

	/**
	 * @param cntrSeqNbr the cntrSeqNbr to set
	 */
	public void setCntrSeqNbr(Long cntrSeqNbr) {
		this.cntrSeqNbr = cntrSeqNbr;
	}
}
