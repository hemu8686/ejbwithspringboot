package sg.com.jp.smartwrapper;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmartConnectorApplication {

	private static final Log log = LogFactory.getLog(SmartConnectorApplication.class);

	@PostConstruct
	void started() {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		log.info(TimeZone.getDefault());
	}

	public static void main(String[] args) {
		SpringApplication.run(SmartConnectorApplication.class, args);

	}

}
